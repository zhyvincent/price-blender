### How to Run
- maven import and Run 'mvn clean install'
- In IntelliJ, mark /test/java as Test Sources Root
- Run Test classes to see results
  
### Source Class Intro
- Price - Wrapper class as value in map, to avoid multiple Double objects created
- PriceBlenderImpl - Please refer to in-class comments for assumptions and details

### Test Class Intro
- PriceBlenderImplTest - Test same values from requirement doc, price aggregation, zero/non-zero values, etc
- PriceBlenderImplConcurrentTest - Simple concurrent test on basic sequence


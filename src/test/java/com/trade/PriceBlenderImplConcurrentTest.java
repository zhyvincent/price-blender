package com.trade;

import org.junit.Before;
import org.junit.Test;

import java.util.concurrent.CompletableFuture;

import static com.trade.MarketSource.*;

/**
 * Simple Concurrent Test
 */
public class PriceBlenderImplConcurrentTest {

    private PriceBlenderImpl unit;

    @Before
    public void before(){
        unit = PriceBlenderImpl.getInstance();
    }

    @Test
    public void testNonZeroValue(){
        CompletableFuture.allOf(
            CompletableFuture.runAsync(() -> { unit.updatePrice(19, 21, SOURCE_A); }),
            CompletableFuture.runAsync(() -> { unit.updatePrice(20, 23, SOURCE_B); }),
            CompletableFuture.runAsync(() -> { unit.updatePrice(18, 22, SOURCE_C); })
        ).thenRun(() -> {
            assert unit.getBestBid() == 20;
            assert unit.getBestAsk() == 21;
            assert unit.getBestMid() == 20.5;
        });
    }

    @Test
    public void testZeroValue(){
        CompletableFuture.allOf(
                CompletableFuture.runAsync(() -> { unit.updatePrice(19, 21, SOURCE_A); }),
                CompletableFuture.runAsync(() -> { unit.updatePrice(20, 23, SOURCE_B); }),
                CompletableFuture.runAsync(() -> { unit.updatePrice(18, 22, SOURCE_C); }),
                CompletableFuture.runAsync(() -> { unit.updatePrice(0, 0, SOURCE_A); }),
                CompletableFuture.runAsync(() -> { unit.updatePrice(0, 0, SOURCE_B); }),
                CompletableFuture.runAsync(() -> { unit.updatePrice(0, 0, SOURCE_C); })
        ).thenRun(() -> {
            assert unit.getBestBid() == 0;
            assert unit.getBestAsk() == 0;
            assert unit.getBestMid() == 0;
        });
    }



}

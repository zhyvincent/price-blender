package com.trade;

import org.junit.Before;
import org.junit.Test;

import static com.trade.MarketSource.*;

public class PriceBlenderImplTest {

    private PriceBlenderImpl unit;

    @Before
    public void before(){
        unit = PriceBlenderImpl.getInstance();
    }

    //Test the price numbers given from Requirement
    @Test
    public void testNonZeroValue(){
        unit.updatePrice(19, 21, SOURCE_A);
        assert unit.getBestBid() == 19;
        assert unit.getBestAsk() == 21;
        assert unit.getBestMid() == 20;

        unit.updatePrice(20, 23, SOURCE_B);
        unit.updatePrice(18, 22, SOURCE_C);
        assert unit.getBestBid() == 20;
        assert unit.getBestAsk() == 21;
        assert unit.getBestMid() == 20.5;
    }

    //When no price from any source, all methods return 0
    @Test
    public void testZeroValue1(){
        assert unit.getBestBid() == 0;
        assert unit.getBestAsk() == 0;
        assert unit.getBestMid() == 0;
    }

    //When 0 price updated by all sources, all methods return 0
    @Test
    public void testZeroValue2(){
        unit.updatePrice(19, 21, SOURCE_A);
        unit.updatePrice(20, 23, SOURCE_B);
        unit.updatePrice(18, 22, SOURCE_C);
        unit.updatePrice(0, 0, SOURCE_A);
        unit.updatePrice(0, 0, SOURCE_B);
        unit.updatePrice(0, 0, SOURCE_C);
        assert unit.getBestBid() == 0;
        assert unit.getBestAsk() == 0;
        assert unit.getBestMid() == 0;
    }

}

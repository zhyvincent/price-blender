package com.trade;

import java.util.EnumMap;
import java.util.Map;
import static com.trade.MarketSource.*;

public class PriceBlenderImpl implements PriceBlender{

    //Initialize Bid/Ask of all sources as 0
    //Use enumMap instead of String map to make less call on toString()
    //Use wrapper Price class. thus we save memory from creating multiple Double object in map by multiple map.put
    private final EnumMap<MarketSource, Price> bidMap = new EnumMap(MarketSource.class){{
        put(SOURCE_A, new Price(0d));
        put(SOURCE_B, new Price(0d));
        put(SOURCE_C, new Price(0d));
    }};
    private final EnumMap<MarketSource, Price> askMap = new EnumMap(MarketSource.class){{
        put(SOURCE_A, new Price(0d));
        put(SOURCE_B, new Price(0d));
        put(SOURCE_C, new Price(0d));
    }};

    //Save bestBid/bestAsk as volatile and allow synchronized updatePrice to update them
    private volatile double bestBid;
    private volatile double bestAsk;
    private PriceBlenderImpl(double bestBid, double bestAsk) {
        this.bestBid = bestBid;
        this.bestAsk = bestAsk;
    }

    //Singleton to save memory
    private static volatile PriceBlenderImpl instance;
    public static PriceBlenderImpl getInstance(){
        if(instance == null){
            synchronized (PriceBlenderImpl.class){
                if(instance == null){
                    instance = new PriceBlenderImpl(0d, 0d);    //init best bid/ask
                }
            }
        }
        return instance;
    }

    @Override
    public double getBestBid() { return this.bestBid; }

    @Override
    public double getBestAsk() {
        return this.bestAsk;
    }

    @Override
    public double getBestMid() {
        return (this.bestBid + this.bestAsk) / 2;
    }

    @Override
    public synchronized void updatePrice(double bid, double ask, MarketSource source) {
        if(bid > ask)     //Simple validation
            return;

        //Compare with other 2 sources' bids to get the MAX bid.
        //if new bid is 0, it is handled also
        double newBestBid = bid;
        for (Map.Entry<MarketSource, Price> entry : bidMap.entrySet()) {
            if (entry.getKey() != source) {
                double existingBid = entry.getValue().get();
                newBestBid = existingBid > newBestBid ? existingBid : newBestBid; //find Max bid
            }
        }
        //Compare with other 2 sources' asks to get the MIN ask
        //if new ask is 0, exclude it from newBestAsk check
        double newBestAsk = ask;
        for (Map.Entry<MarketSource, Price> entry : askMap.entrySet()) {
            if (entry.getKey() != source) {
                double existingAsk = entry.getValue().get();
                if(newBestAsk == 0){
                    newBestAsk = existingAsk; continue;
                }
                newBestAsk = existingAsk > newBestAsk ||  existingAsk == 0 ? newBestAsk : existingAsk; //find min ask
            }
        }

        //Only when no price cross, we update map and best bid/ask; Handle price 0
        if(newBestBid < newBestAsk || (newBestBid == 0 && newBestAsk == 0)){
            bestBid = newBestBid;
            bestAsk = newBestAsk;
            Price bidPrice = bidMap.get(source);
            bidPrice.set(bid);
            Price askPrice = askMap.get(source);
            askPrice.set(ask);
        }

    }

}

package com.trade;

public class Price {

    public Price(double value) {
        this.value = value;
    }

    private double value;

    public double get() {
        return value;
    }

    public void set(double value) {
        this.value = value;
    }
}
